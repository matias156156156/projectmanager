﻿using EscuelaNet.Aplicacion.Clientes.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public interface IUnidadesQuery
    {
        UnidadesQueryModel GetUnidadDeNegocio(int id);

        List<UnidadesQueryModel> ListUnidades(int id);

        List<UnidadesQueryModel> ListTodasUnidades();

        List<SolicitudesQueryModel> ListSolicitudesDeUnidad(int id);

        SolicitudesQueryModel FindSolicitud(int id, int unidad);

    }
}

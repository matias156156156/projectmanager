﻿using EscuelaNet.Dominio.Capacitaciones;
using System.Collections.Generic;

namespace EscuelaNet.Presentacion.Capacitaciones.Web.Models
{
    public class InstructorIndexModel
    {
        public string Nombre { get; set; }

        public List<Instructor> Instructores { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Autofac;
using EscuelaNet.Dominio.Programadores;
using EsculaNet.Infraestructura.Programadores;
using EsculaNet.Infraestructura.Programadores.Repositorios;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var entorno = ConfigurationManager.AppSettings["Entorno"];
            switch(entorno)
            {
                case "Produccion":
                    var connectionString = 
                        ConfigurationManager
                            .ConnectionStrings["EquipoContext"].ToString();
                    builder.RegisterType<EquipoContext>()
                        .InstancePerRequest();
                    //builder.Register(c => new TemasQuery(
                    //        connectionString))
                    //    .As<ITemasQuery>()
                    //    .InstancePerLifetimeScope();
                    builder.RegisterType<EquiposRepository>()
                        .As<IEquipoRepository>()
                        .InstancePerLifetimeScope();
                    builder.RegisterType<SkillsRepository>()
                        .As<ISkillRepository>()
                        .InstancePerLifetimeScope();
                    builder.RegisterType<ProgramadoresRepository>()
                       .As<IProgramadorRepository>()
                       .InstancePerLifetimeScope();
                    break;
                case "Singleton":
                default:
                    //builder.RegisterType<LugaresSingletonRepository>()
                    //    .As<ILugarRepository>()
                    //    .InstancePerLifetimeScope();
                    break;

            }

            base.Load(builder);
        }
    }
}
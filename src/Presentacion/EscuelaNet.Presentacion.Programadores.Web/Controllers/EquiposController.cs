﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.QueryServices;
using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using EscuelaNet.Infraestructura.Programadores.Repositorios;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class EquiposController : Controller
    {
        private IEquipoRepository _repositorio;
        private ISkillRepository _repositorioSKill;
        private IProgramadorRepository _repositorioProgramador;
        private IEquipoQuery _equipoQuery;
        private IMediator _mediator;

        public EquiposController(IEquipoRepository equipoRepository,ISkillRepository skillRepository,
            IProgramadorRepository programadorRepository, IEquipoQuery equipoQuery, IMediator mediator)
        {
            _repositorio = equipoRepository;
            _repositorioSKill = skillRepository;
            _repositorioProgramador = programadorRepository;
            _equipoQuery = equipoQuery;
            _mediator = mediator;
        }
        // GET: Equipos
        public ActionResult Index()
        {
            //var equipos = _repositorio.ListEquipo();
            var equipos = _equipoQuery.ListEquipo();

            var model = new EquiposIndexModel()
            {
                Titulo = "Prueba de Equipos",
                Equipos = equipos
            };
            return View(model);
        }
        public ActionResult New()
        {
            var model = new NuevoEquipoModel();
            return View(model);

        }

        [HttpPost]
        public async Task<ActionResult> New(NuevoEquipoCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Equipo creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoEquipoModel()
                {
                    Nombre = model.Nombre,
                    Pais = model.Pais,
                    HusoHorario = model.HusoHorario
                };
                return View(modelReturn);
            }
        }
        //public ActionResult New(NuevoEquipoModel model)
        //{
        //    if (!string.IsNullOrEmpty(model.Nombre))
        //    {
        //        try
        //        {
        //            var equipo = new Equipo(model.Nombre, model.Pais, model.HusoHorario);
        //            _repositorio.Add(equipo);

        //            _repositorio.UnitOfWork.SaveChanges();

        //            TempData["success"] = "Equipo Creado Correctamente";
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception ex)
        //        {
        //            TempData["error"] = ex.Message;
        //            return View(model);
        //        }

        //    }
        //    else
        //    {
        //        TempData["error"] = "Texto vacio";
        //        return View(model);

        //    }

        //}
        public ActionResult Edit(int id)
        {
            var equipo = _equipoQuery.GetEquipo(id);

            var model = new NuevoEquipoModel()
            {
                Nombre = equipo.Nombre,
                IdEquipo = id,
                Pais = equipo.Pais,
                HusoHorario = equipo.HusoHorario
            };
            return View(model);
        }
        //public ActionResult Edit(int id)
        //{
        //    if (id <= 0)
        //    {
        //        TempData["error"] = "Id del Equipo no valido";
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        var equipo = _repositorio.GetEquipo(id);
        //        var model = new NuevoEquipoModel()
        //        {
        //            Nombre = equipo.Nombre,
        //            Pais = equipo.Pais,
        //            HusoHorario = equipo.HusoHorario,
        //            IdEquipo = id
        //        };
        //        return View(model);
        //    }
        //}

        [HttpPost]
        public ActionResult Edit(NuevoEquipoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var equipo = _repositorio.GetEquipo(model.IdEquipo);
                    equipo.Nombre = model.Nombre;
                    equipo.Pais = model.Pais;
                    equipo.HusoHorario = model.HusoHorario;

                    _repositorio.Update(equipo);
                    _repositorio.UnitOfWork.SaveChanges();


                    TempData["success"] = "Tema Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }
        //public ActionResult Edit(NuevoEquipoModel model)
        //{
        //    if (!string.IsNullOrEmpty(model.Nombre))
        //    {
        //        try
        //        {
        //            var equipo = _repositorio.GetEquipo(model.IdEquipo);
        //            equipo.Nombre = model.Nombre;
        //            equipo.Pais = model.Pais;
        //            equipo.HusoHorario = model.HusoHorario;

        //            _repositorio.Update(equipo);
        //            _repositorio.UnitOfWork.SaveChanges();

        //            TempData["success"] = "Equipo Editado Correctamente";
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception ex)
        //        {
        //            TempData["error"] = ex.Message;
        //            return View(model);
        //        }

        //    }
        //    else
        //    {
        //        TempData["error"] = "Texto vacio";
        //        return View(model);

        //    }

        //}
        public ActionResult Delete(int id)
        {
            var equipo = _repositorio.GetEquipo(id);

            var model = new NuevoEquipoModel()
            {
                Nombre = equipo.Nombre,
                Pais = equipo.Pais,
                HusoHorario = equipo.HusoHorario,
                IdEquipo = id
            };
            return View(model);
        }
        //public ActionResult Delete(int id)
        //{
        //    if (id <= 0)
        //    {
        //        TempData["error"] = "Id del Equipo no valido";
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        var equipo = _repositorio.GetEquipo(id);
        //        var model = new NuevoEquipoModel()
        //        {
        //            Nombre = equipo.Nombre,
        //            Pais = equipo.Pais,
        //            HusoHorario = equipo.HusoHorario,
        //            IdEquipo = id
        //        };

        //        return View(model);
        //    }
        //}

        [HttpPost]
        public ActionResult Delete(NuevoEquipoModel model)
        {
            try
            {
                var equipo = _repositorio.GetEquipo(model.IdEquipo);
                _repositorio.Delete(equipo);
                _repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Equipo borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        //public ActionResult Delete(NuevoEquipoModel model)
        //{

        //    try
        //    {
        //        var equipo = _repositorio.GetEquipo(model.IdEquipo);
        //        _repositorio.Delete(equipo);
        //        _repositorio.UnitOfWork.SaveChanges();

        //        TempData["success"] = "Equipo Borrado Correctamente";
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["error"] = ex.Message;
        //        return View(model);
        //    }
        //}

        public ActionResult Skills(int id)
        {
            var equipo = _repositorio.GetEquipo(id);
            var model = new EquipoSkillModel()
            {
                Equipo = equipo,
                Skills = equipo.Skills.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteSkills(int id, int idEquipo)
        {
            var equipoBuscado = _repositorio.GetEquipo(idEquipo);
            var skillBuscado = equipoBuscado.Skills.First(s => s.ID == id);

            var model = new NuevoEquipoSkillModel()
            {
                DescripcionSkill = skillBuscado.Descripcion + " " + skillBuscado.Grados,
                IdEquipo = idEquipo,
                NombreEquipo = equipoBuscado.Nombre,
                IdSkill = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSkills(NuevoEquipoSkillModel model)
        {
            try
            {
                var equipoBuscado = _repositorio.GetEquipo(model.IdEquipo);
                var skillBuscado = equipoBuscado.Skills.First(s => s.ID == model.IdSkill);
                equipoBuscado.RemoveSkill(skillBuscado);

                _repositorio.Update(equipoBuscado);
                _repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        public ActionResult NewSkill(int id)
        {
            var equipo = _repositorio.GetEquipo(id);
            var skill = _repositorioSKill.ListSkill();

            var model = new NuevoEquipoSkillModel()
            {
                IdEquipo = id,
                NombreEquipo = equipo.Nombre,
                Skills = skill
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewSkill(NuevoEquipoSkillModel model)
        {
            try
            {
                var equipoBuscado = _repositorio.GetEquipo(model.IdEquipo);
                var skillBuscado = _repositorioSKill.GetSkill(model.IdSkill);

                equipoBuscado.PushSkill(skillBuscado);

                _repositorio.Update(equipoBuscado);
                _repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Skill Agregado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

        public ActionResult Programadores(int id)
        {
            var equipo = _repositorio.GetEquipo(id);
            var model = new EquipoProgramadorModel()
            {
                Equipo = equipo,
                Programadores = equipo.Programadores.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteProgramador(int id, int idEquipo)
        {
            var equipoBuscado = _repositorio.GetEquipo(idEquipo);
            var programadorBuscado = equipoBuscado.Programadores.First(p => p.ID == id);
            var model = new NuevoEquipoProgramadorModel()
            {
                NombreProgramador = programadorBuscado.Nombre + " " + programadorBuscado.Apellido,
                IdEquipo = idEquipo,
                NombreEquipo = equipoBuscado.Nombre,
                IdProgramador = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteProgramador(NuevoEquipoProgramadorModel model)
        {
            try
            {
                var equipoBuscado = _repositorio.GetEquipo(model.IdEquipo);
                var programadorBuscado = equipoBuscado.Programadores.First(s => s.ID == model.IdProgramador);

                var programador = _repositorioProgramador.GetProgramador(programadorBuscado.ID);
                equipoBuscado.updateProgramador(programadorBuscado);
                equipoBuscado.CantidadHorasDisponibleProgramadores();

                _repositorioProgramador.Delete(programador);
                _repositorioProgramador.UnitOfWork.SaveChanges();

                _repositorio.Update(equipoBuscado);
                _repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Programador borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewProgramador(int id)
        {
            var equipo = _repositorio.GetEquipo(id);
            var programador = _repositorioProgramador.ListProgramador();

            var model = new NuevoEquipoProgramadorModel()
            {
                IdEquipo = id,
                NombreEquipo = equipo.Nombre,
                Programadores = programador
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewProgramador(NuevoEquipoProgramadorModel model)
        {
            try
            {
                var equipoBuscado = _repositorio.GetEquipo(model.IdEquipo);
                var programadorBuscado = _repositorioProgramador.GetProgramador(model.IdProgramador);

                foreach (var conocimiento in equipoBuscado.Skills)
                {
                    if (!programadorBuscado.VerificarConocimiento(conocimiento.Descripcion))
                    {
                        TempData["error"] = "No contiene los conocimientos necesarios para este equipo";
                        return RedirectToAction("NewProgramador", new { model.IdEquipo });
                    }
                }

                if (!equipoBuscado.Programadores.Contains(programadorBuscado))
                {
                    equipoBuscado.PushProgramador(programadorBuscado);

                    _repositorio.Update(equipoBuscado);
                    _repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Programador Agregado";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["error"] = "Ya se encuentra el programador";
                    return RedirectToAction("NewProgramador", new { model.IdEquipo });
                }

            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}